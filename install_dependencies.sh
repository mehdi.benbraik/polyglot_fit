#! /bin/sh
# This script will install, in Fedora, all the dependencies for the examples

sudo dnf install python3 python3-numpy python3-scipy python3-matplotlib

sudo dnf install octave

sudo dnf install R

sudo dnf install clang gnuplot gsl gsl-devel

sudo dnf install nodejs npm
npm install csv-parser simple-statistics regression d3-node

sudo dnf install racket
raco pkg install csv-reading
raco pkg install plot-bestfit
raco pkg install plot-gui-lib

