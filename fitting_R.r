# Copyright (c) 2019 Cristiano L. Fontana
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

inputFileName <- "anscombe.csv"
delimiter <- "\t"
skipHeader <- 3
columnX <- 1
columnY <- 2

cat("#### Anscombe's first set with R ####\n")

rawData <- read.csv(file=inputFileName, header=FALSE, sep=delimiter, skip=skipHeader)

data <- rawData[, c(columnX, columnY)]

colnames(data) <- c("x","y")

linearModel <- lm(y ~ x, data=data)

intercept <- linearModel$coefficients[1]
slope <- linearModel$coefficients[2]

cat("Slope: ", format(slope), "\n")
cat("Intercept: ", format(intercept), "\n")

r_value <- cor(data$x, data$y)

cat("Correlation coefficient: ", format(r_value), "\n")

figDPI <- 100
figWidth <- 7.0
figHeight <- figWidth / 16 * 9

png("fit_R.png", width=figWidth, height=figHeight, units="in", res=figDPI)

plot(data$x, data$y, xlab="X", ylab="Y", pch=16)
abline(linearModel, lty=1)

legend(min(data$x), max(data$y), legend=c("Data", "Fit"), pch=c(16, NA), lty=c(NA, 1))

garbage <- dev.off()
