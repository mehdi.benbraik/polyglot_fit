# The polyglot programmer's way of fitting and plotting data.

This is a collection of examples in several programming languages.
All the programs load the included [CSV file](./anscombe.csv), fit the data with a straight line and then plot the result creating a PNG file.
It is not intended to be an exhaustive comparison of the languages, but just a little showcase.
The readers that are more familiar with one of the languages, can start from that one and go through the others looking for analogies and differences.

## Installation
The [install_dependencies.sh](./install_dependencies.sh) script, installs all the needed packages for Fedora.

## Running the examples
The provided [Makefile](./Makefile) can be used to run all the examples in sequence.

## Ubuntu 19.10 instrunctions
sudo apt install clang gnuplot gsl-bin libgsl-dev libcsv-dev valgrind gnuplot
gcc -g -O0 -o fitting my_fitting_C99.c -lcsv -lgsl
valgrind -s --undef-value-errors=no --leak-check=yes ./fitting

### libcsv
CSV <http://manpages.ubuntu.com/manpages/bionic/man3/csv.3.html>

### indent
indent <https://en.wikipedia.org/wiki/Indent_%28Unix%29#Examples_of_usage>
$ indent -st -bap -bli0 -i4 -l79 -ncs -npcs -npsl -fca -lc79 -fc1 -ts4 some_file.c

### Get values from strings without NULL terminated
Using printf with a non-null terminated string <https://stackoverflow.com/questions/3767284/using-printf-with-a-non-null-terminated-string>
printf("%.*s", stringLength, pointerToString);

### Bit Masks
Bit Masks for Better Code <http://learn.parallax.com/tutorials/language/propeller-c/propeller-c-simple-protocols/spi-example/bit-masks-better-code>

### Valgrind
https://valgrind.org/
Valgrind is an instrumentation framework for building dynamic analysis tools.
There are Valgrind tools that can automatically detect many memory management 
and threading bugs, and profile your programs in detail.
You can also use Valgrind to build new tools.

### Valgrind - Ignore libs
Is it possible to make valgrind ignore certain libraries? <https://stackoverflow.com/questions/3174468/is-it-possible-to-make-valgrind-ignore-certain-libraries>

### Boolean datatype
boolean (bool or _Bool) datatype in C <https://iq.opengenus.org/boolean-in-c/>

### Disable GCC warnings
How to disable GCC warnings for a few lines of code <https://stackoverflow.com/questions/3378560/how-to-disable-gcc-warnings-for-a-few-lines-of-code>

```
#pragma GCC diagnostic error "-Wuninitialized"
            foo(a);         /* error is given for this one */
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wuninitialized"
            foo(b);         /* no diagnostic for this one */
#pragma GCC diagnostic pop
            foo(c);         /* error is given for this one */
#pragma GCC diagnostic pop
            foo(d);         /* depends on command line options */
```

